package topologytraversal.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Path {

    @JacksonXmlProperty
    private Route route;

    @JacksonXmlElementWrapper(localName = "entities")
    private Clazz[] entities;

    @JacksonXmlProperty
    private Association[] associations;

    public Route getRoute() {
        return route;
    }

    public Clazz[] getEntities() {
        return entities;
    }

    public Association[] getAssociations() {
        return associations;
    }
}
