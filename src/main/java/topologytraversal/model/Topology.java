package topologytraversal.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement()
public class Topology {

    @JacksonXmlElementWrapper(localName = "entities")
    private List<Clazz> entities;

    @JacksonXmlProperty
    private List<Association> associations;

    public List<Clazz> getEntities() {
        return entities;
    }

    public List<Association> getAssociations() {
        return associations;
    }
}
