package topologytraversal.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Clazz {

    @JacksonXmlProperty(localName = "key", isAttribute = true)
    private String key;

    @JacksonXmlElementWrapper(localName = "entity", useWrapping = false)
    private List<Entity> entity;

    public Clazz() {
        // for serialization
    }

    public Clazz(String key, List<Entity> entity) {
        this.key = key;
        this.entity = entity;
    }

    public String getKey() {
        return key;
    }

    public List<Entity> getEntity() {
        return entity;
    }
}
