package topologytraversal.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.collect.Lists;
import topologytraversal.pathfinder.TopoNode;

import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class Route {

    @JacksonXmlProperty(isAttribute = true)
    private boolean valid;

    @JacksonXmlProperty(isAttribute = true)
    private String start;
    @JacksonXmlProperty(isAttribute = true)
    private String end;

    @JacksonXmlElementWrapper(localName = "entities")
    @JacksonXmlProperty(localName = "class")
    private List<Clazz> entities;

    @JacksonXmlElementWrapper(localName = "associations")
    @JacksonXmlProperty(localName = "association")
    private List<Association> associations;

    public Route(boolean valid, String start, String end, List<Clazz> entities, List<Association> associations) {
        this.valid = valid;
        this.start = start;
        this.end = end;

        this.entities = entities;
        this.associations = associations;
    }

    public boolean getValid() {
        return valid;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public List<Clazz> getEntities() {
        return entities;
    }

    public List<Association> getAssociations() {
        return associations;
    }

    public static Route invalid(String start, String end) {
        return new Route(false, start, end, Lists.newArrayList(), Lists.newArrayList());
    }

    public static Route valid(String start, String end, Topology topology, Stack<TopoNode> nodes) {
        List<Clazz> entities = Lists.newArrayList();

        List<String> toKeep = nodes.stream()
                .map(TopoNode::getKey)
                .collect(Collectors.toList());

        for (Clazz c : topology.getEntities()) {
            List<Entity> filtered = c.getEntity().stream()
                    .filter(e -> toKeep.contains(e.getKey()))
                    .collect(Collectors.toList());

            entities.add(new Clazz(c.getKey(), filtered));
        }

        List<Association> associations = Lists.newArrayList();
        TopoNode f = nodes.pop();
        while(!nodes.isEmpty()) {
            TopoNode t = nodes.pop();
            associations.add(new Association(f.getKey(), t.getKey()));
            f = t;
        }

        return new Route(true, start, end, entities, associations);
    }


}
