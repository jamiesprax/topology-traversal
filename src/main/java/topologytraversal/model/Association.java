package topologytraversal.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Association {

    @JacksonXmlProperty(isAttribute = true)
    private String primary;
    @JacksonXmlProperty(isAttribute = true)
    private String secondary;

    public Association() {
        // For serialization
    }

    public Association(String primary, String secondary) {
        this.primary = primary;
        this.secondary = secondary;
    }

    public String getPrimary() {
        return primary;
    }

    public String getSecondary() {
        return secondary;
    }
}
