package topologytraversal.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Entity {

    @JacksonXmlProperty(isAttribute = true)
    private String key;

    public Entity() {
        // for serialization
    }

    public String getKey() {
        return key;
    }
}
