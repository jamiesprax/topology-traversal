package topologytraversal;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import topologytraversal.parser.TopologyParser;
import topologytraversal.parser.XmlTopologyParser;
import topologytraversal.pathfinder.TopologyPathFinder;
import topologytraversal.resources.TopologyTraverseResource;

public class TopologyTraversalApplication extends Application<TopologyTraversalConfiguration> {

    public static void main(String[] args) throws Exception {
        new TopologyTraversalApplication().run(args);
    }

    public void run(TopologyTraversalConfiguration config, Environment env) {

        ObjectMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        TopologyParser parser = new XmlTopologyParser(mapper);
        TopologyPathFinder pathFinder = new TopologyPathFinder();

        TopologyTraverseResource resource = new TopologyTraverseResource(mapper, parser, pathFinder);

        env.jersey().register(MultiPartFeature.class);
        env.jersey().register(resource);
    }
}
