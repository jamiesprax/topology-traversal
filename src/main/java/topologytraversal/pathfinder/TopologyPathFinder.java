package topologytraversal.pathfinder;

import topologytraversal.model.Route;
import topologytraversal.model.Topology;
import topologytraversal.parser.TopologyParser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;

public class TopologyPathFinder {

    public Route findBFSRouteFor(Topology topology, String from, String to) {

        HashMap<String, TopoNode> nodes = TopologyParser.transform(topology);

        if (!nodes.containsKey(from) || !nodes.containsKey(to)) {
            return Route.invalid(from, to);
        }

        LinkedList<TopoNode> queue = new LinkedList<>();
        HashMap<String, Boolean> visited = new HashMap<>();
        HashMap<String, Integer> distance = new HashMap<>();
        HashMap<String, String> predecessor = new HashMap<>();

        for(TopoNode node : nodes.values()) {
            visited.put(node.getKey(), false);
            distance.put(node.getKey(), Integer.MAX_VALUE);
            predecessor.put(node.getKey(), "");
        }

        visited.put(from, true);
        distance.put(from, 0);
        queue.add(nodes.get(from));

        boolean pathFound = false;

        while(!queue.isEmpty()) {
            TopoNode curr = queue.pop();
            for (TopoNode n : curr.getForwardNodes()) {
                if (!visited.get(n.getKey())) {
                    visited.put(n.getKey(), true);
                    distance.put(n.getKey(), distance.get(curr.getKey()) + 1);
                    predecessor.put(n.getKey(), curr.getKey());
                    queue.add(n);

                    if (n.getKey().equals(to)) {
                        pathFound = true;
                    }
                }
            }
        }

        if (!pathFound) {
            return Route.invalid(from, to);
        }

        Stack<TopoNode> path = new Stack<>();
        String back = to;
        path.add(nodes.get(back));
        while (!back.equals(from)) {
            String tBack = predecessor.get(back);
            path.push(nodes.get(tBack));
            back = tBack;
        }

        return Route.valid(from, to, topology, path);
    }

}
