package topologytraversal.pathfinder;

import java.util.HashSet;
import java.util.Set;

public class TopoNode {

    private String key;
    private Set<TopoNode> forwardNodes;

    public TopoNode(String key) {
        this.key = key;
        forwardNodes = new HashSet<>();
    }

    public String getKey() {
        return key;
    }

    public Set<TopoNode> getForwardNodes() {
        return forwardNodes;
    }

    public void addForwardNode(TopoNode node) {
        forwardNodes.add(node);
    }
}
