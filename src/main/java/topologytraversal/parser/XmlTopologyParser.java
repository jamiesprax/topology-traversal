package topologytraversal.parser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import topologytraversal.model.Topology;

public class XmlTopologyParser extends TopologyParser {

    private ObjectMapper mapper;

    public XmlTopologyParser(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public Topology parse(String topoString) {
        try {
            return mapper.readValue(topoString, Topology.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
