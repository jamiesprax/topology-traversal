package topologytraversal.parser;

import topologytraversal.model.Association;
import topologytraversal.model.Topology;
import topologytraversal.pathfinder.TopoNode;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

public abstract class TopologyParser {

    Logger log = Logger.getLogger("TopologyParser");

    public Topology parseFile(String filePath) {
        try(BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return parse(sb.toString());
        } catch (IOException e) {
            log.severe(String.format("Parsing of '%s' failed", filePath));
            e.printStackTrace();
            return null;
        }
    }

    public abstract Topology parse(String topoString);

    public static HashMap<String, TopoNode> transform(Topology topology) {
        HashMap<String, TopoNode> nodeMap = new HashMap<>();

        for (Association association : topology.getAssociations()) {
            TopoNode primary = nodeMap.getOrDefault(association.getPrimary(), new TopoNode(association.getPrimary()));
            TopoNode secondary = nodeMap.getOrDefault(association.getSecondary(), new TopoNode(association.getSecondary()));

            primary.addForwardNode(secondary);

            nodeMap.putIfAbsent(association.getPrimary(), primary);
            nodeMap.putIfAbsent(association.getSecondary(), secondary);
        }

        return nodeMap;
    }
}
