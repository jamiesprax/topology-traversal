package topologytraversal.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import topologytraversal.model.Route;
import topologytraversal.model.Topology;
import topologytraversal.parser.TopologyParser;
import topologytraversal.parser.XmlTopologyParser;
import topologytraversal.pathfinder.TopologyPathFinder;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Path("/1/topology")
@Produces(MediaType.APPLICATION_XML)
public class TopologyTraverseResource {

    private ObjectMapper mapper;
    private TopologyParser topologyParser;
    private TopologyPathFinder pathFinder;

    public TopologyTraverseResource(ObjectMapper mapper, TopologyParser topologyParser, TopologyPathFinder pathFinder) {
        this.mapper = mapper;
        this.topologyParser = topologyParser;
        this.pathFinder = pathFinder;
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream file,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @QueryParam("from") String from,
            @QueryParam("to") String to
    ) {
        try {
            String filepath = System.currentTimeMillis() + ".topology";
            saveFile(file, filepath);

            Topology topo = topologyParser.parseFile(filepath);
            Route route = pathFinder.findBFSRouteFor(topo, from, to);

            File f = new File(filepath.replace("topology", "route"));
            FileWriter fw = new FileWriter(f);
            fw.write(mapper.writeValueAsString(route));
            fw.flush();
            fw.close();

            return Response.accepted(mapper.writeValueAsString(route)).build();
        } catch (IOException e) {
            return Response.status(500, e.getMessage()).build();
        }
    }

    private void saveFile(InputStream uploadedInputStream, String filepath) {
        try (OutputStream outputStream = new FileOutputStream(new File(filepath))) {
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = uploadedInputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
