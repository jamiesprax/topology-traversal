package topologytraversal.pathfinder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import org.junit.Before;
import org.junit.Test;
import topologytraversal.model.Route;
import topologytraversal.model.Topology;
import topologytraversal.parser.XmlTopologyParser;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class TopologyPathFinderTest {

    private ObjectMapper mapper;
    private XmlTopologyParser parser;
    private TopologyPathFinder topologyPathFinder;

    @Before
    public void setUp() {
        mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        parser = new XmlTopologyParser(mapper);

        topologyPathFinder = new TopologyPathFinder();
    }

    @Test
    public void findRoute() {
        Topology topology = parser.parseFile("src/main/resources/topoTestA.topology");

        Route route = topologyPathFinder.findBFSRouteFor(topology, "T/2345", "T/0032");

        assertTrue(route.getValid());
        assertThat(route.getAssociations().size(), is(4));
    }

    @Test
    public void findShortestRoute() {
        Topology topology = parser.parseFile("src/main/resources/topoTestB.topology");

        Route route = topologyPathFinder.findBFSRouteFor(topology, "T/2345", "T/0032");

        assertTrue(route.getValid());
        assertThat(route.getAssociations().size(), is(2));
    }
}