package topologytraversal.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Before;
import org.junit.Test;
import topologytraversal.model.Topology;
import topologytraversal.pathfinder.TopoNode;

import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class XmlTopologyParserTest {

    private ObjectMapper mapper;
    private XmlTopologyParser parser;

    @Before
    public void setUp() {
        mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        parser = new XmlTopologyParser(mapper);
    }

    @Test
    public void parseFile() {
        Topology topology = parser.parseFile("src/main/resources/topoTestA.topology");

        assertThat(topology.getEntities().size(), is(3));
        assertThat(topology.getAssociations().size(), is(8));

        assertThat(topology.getEntities().get(0).getKey(), is("Transceiver"));
        assertThat(topology.getEntities().get(1).getKey(), is("Link"));
        assertThat(topology.getEntities().get(2).getKey(), is("Fibre"));

        assertThat(topology.getEntities().get(0).getEntity().size(), is(4));
        assertThat(topology.getEntities().get(1).getEntity().size(), is(3));
        assertThat(topology.getEntities().get(2).getEntity().size(), is(2));

        assertThat(topology.getAssociations().get(0).getPrimary(), is("T/2345"));
        assertThat(topology.getAssociations().get(0).getSecondary(), is("Bartrum-X5"));
    }

    @Test
    public void parseTopology() {
        Topology topology = parser.parseFile("src/main/resources/topoTestA.topology");

        HashMap<String, TopoNode> nodeMap = parser.transform(topology);

        assertThat(nodeMap.keySet().size(), is(9));
    }
}